﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using UCA.WebCalculator.Api;

namespace UCA.WebCalculator.Tests
{
    [TestClass]
    public class CalculatorFixture
    {
        [TestInitialize]
        public void OnTestInitialize()
        {
            _SystemUnderTest = null;
        }
        
        private Calculator _SystemUnderTest;
        public Calculator SystemUnderTest
        {
            get
            {
                if (_SystemUnderTest == null)
                {
                    _SystemUnderTest = new Calculator();
                }

                return _SystemUnderTest;
            }
        }

        [TestMethod]
        public void Add()
        {
            // arrange
            double value1 = 2;
            double value2 = 3;
            double expected = 5;

            // act
            double actual = SystemUnderTest.Add(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }


        [TestMethod]
        public void Subtract()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 4;

            // act
            double actual = SystemUnderTest.Subtract(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        public void Multiply()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 12;

            // act
            double actual = SystemUnderTest.Multiply(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        public void Divide()
        {
            // arrange
            double value1 = 6;
            double value2 = 2;
            double expected = 3;

            // act
            double actual = SystemUnderTest.Divide(
                value1, value2);

            // assert
            Assert.AreEqual<double>(expected, actual, "Wrong result.");
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void DivideByZeroThrowsException()
        {
            // arrange
            double value1 = 6;
            double value2 = 0;

            // act
            double actual = SystemUnderTest.Divide(
                value1, value2);
        }

        private IWebDriver driver;
        public string homeUrl;

        [TestMethod]
        public void TestSite()
        {
            driver = new ChromeDriver(@"C:\Users\azure\Documents\chromedriver\");
            homeUrl = "https://localhost:44348/Calculator";

            driver.Navigate().GoToUrl(homeUrl);

            driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys("1");

            var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
            var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

            selectElementa.SelectByValue("Add");

            driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys("1");

            driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();

            driver.Close();

        }

        [TestMethod]
        public void TestMultiply()
        {
            driver = new ChromeDriver(@"C:\Users\azure\Documents\chromedriver\");
            homeUrl = "https://localhost:44348/Calculator";

            driver.Navigate().GoToUrl(homeUrl);

            driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys("2");

            var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
            var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

            selectElementa.SelectByValue("Multiply");

            driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys("1");

            driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();

            driver.Close();

        }

        [TestMethod]
        public void TestDivide()
        {
            driver = new ChromeDriver(@"C:\Users\azure\Documents\chromedriver\");
            homeUrl = "https://localhost:44348/Calculator";

            driver.Navigate().GoToUrl(homeUrl);

            driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys("2");

            var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
            var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

            selectElementa.SelectByValue("Divide");

            driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys("1");

            driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();

            driver.Close();

        }

        [TestMethod]
        public void TestSubstract()
        {
            driver = new ChromeDriver(@"C:\Users\azure\Documents\chromedriver\");
            homeUrl = "https://localhost:44348/Calculator";

            driver.Navigate().GoToUrl(homeUrl);

            driver.FindElement(By.XPath("//*[@id='Value1']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value1']")).SendKeys("2");

            var a = driver.FindElement(By.XPath("//*[@id='Operator']"));
            var selectElementa = new OpenQA.Selenium.Support.UI.SelectElement(a);

            selectElementa.SelectByValue("Substract");

            driver.FindElement(By.XPath("//*[@id='Value2']")).Clear();
            driver.FindElement(By.XPath("//*[@id='Value2']")).SendKeys("1");

            driver.FindElement(By.XPath("/html/body/div/form/div/div/div[4]/input")).Click();

            driver.Close();

        }


    }


}
